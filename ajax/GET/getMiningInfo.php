<?php
  $result = $bitcoin->getmininginfo();
  if(!empty($result)){
    $apiOutput = array(
      "status" => 200,
      "result" => array(
        "nodeblocks" => $result['blocks'],
		"netblocks" => file_get_contents("https://chainz.cryptoid.info/xmg/api.dws?q=getblockcount"),
        "difficulty" => array(
          "pow" => $result['difficulty']['proof-of-work'],
          "pos" => $result['difficulty']['proof-of-stake']
        ),
        "blockValue" => $result['blockvalue']['blockvalue']
      )
    );
  }else{
    $apiOutput = array("status" => 500, "result" => null);
  }
