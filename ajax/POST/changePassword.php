<?php
  if(!empty($_POST['old-password'])){
    if(!empty($_POST['new-password'])){
      if(!empty($_SESSION['wallet-identifier']) && !empty($_SESSION['Authtoken'])){
        $sql = "SELECT * FROM `Wallets` WHERE `Wallet-Identifier`='".$conn->escape_string($_SESSION['wallet-identifier'])."' AND `Authtoken`='".$conn->escape_string($_SESSION['Authtoken'])."';";
        if($conn->query($sql)->num_rows > 0){
          $sql = "UPDATE `Wallets` SET `Password`='".$conn->escape_string(password_hash($_POST['password'],PASSWORD_DEFAULT))."' WHERE `Wallet-Identifier`='".$conn->escape_string($_SESSION['wallet-identifier'])."' AND `Authtoken`='".$conn->escape_string($_SESSION['Authtoken'])."';";
          if($conn->query($sql)){
            $apiOutput = array("status" => 200, "message" => "Password has been updated!");
          }else{
            $apiOutput = array("status" => 500, "message" => "Could not update password!");
          }
        }else{
          $apiOutput = array("status" => 403, "message" => "Invalid Wallet Identifier/Authtoken!");
        }
      }else{
        $apiOutput = array("status" => 403, "message" => "Wallet Identifier can't be blank!");
      }
    }else{
      $apiOutput = array("status" => 403, "message" => "New password cannot be left blank!");
    }
  }else{
    $apiOutput = array("status" => 403, "message" => "Invalid password!");
  }
