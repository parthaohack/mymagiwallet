<?php
  if(!empty($_SESSION['wallet-identifier']) && !empty($_SESSION['Authtoken'])){
    $sql = "SELECT * FROM `Wallets` WHERE `Wallet-Identifier`='".$conn->escape_string($_SESSION['wallet-identifier'])."' AND `Authtoken`='".$conn->escape_string($_SESSION['Authtoken'])."';";
    if($conn->query($sql)->num_rows > 0){
      $transactionData = array();
      $transactions = $bitcoin->listtransactions($_SESSION['wallet-identifier']);
      foreach($transactions as $transaction => $data){
        $transactionData['transactions'][] = array(
          "txid" => $data['txid'],
          "blockHash" => $data['blockhash'],
          "address" => $data['address'],
          "timestamp" => $data['time'],
          "confirmations" => $data['confirmations'],
          "category" => $data['category'],
          "amount" => $data['amount']
        );
      }
      $apiOutput = array("status" => 200, "result" => $transactionData);
    }else{
        $apiOutput = array("status" => 403, "message" => "Invalid Wallet Identifier/Authtoken!");
    }
  }else{
    $apiOutput = array("status" => 403, "message" => "Wallet Identifier can't be blank!");
  }
