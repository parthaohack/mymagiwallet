<?php
  if(!empty($_POST['key'])){
    if(!empty($_SESSION['wallet-identifier']) && !empty($_SESSION['Authtoken'])){
      $sql = "SELECT * FROM `Wallets` WHERE `Wallet-Identifier`='".$conn->escape_string($_SESSION['wallet-identifier'])."' AND `Authtoken`='".$conn->escape_string($_SESSION['Authtoken'])."';";
      if($conn->query($sql)->num_rows > 0){
        $result = $bitcoin->importprivkey($_POST['key'],$_SESSION['wallet-identifier'],false);
        $bitcoin->walletpassphrase($appConfig['Wallet']['Key'],60);
        if($result){
          $apiOutput = array("status" => 200, "message" => "Private key has been imported!");
        }else{
          $apiOutput = array("status" => 500, "message" => "Could not import privatekey! (already taken by another account?)");
        }
      }else{
        $apiOutput = array("status" => 403, "message" => "Invalid Wallet Identifier/Authtoken!");
      }
    }else{
      $apiOutput = array("status" => 403, "message" => "Invalid Wallet Identifier/Authtoken!");
    }
  }else{
    $apiOutput = array("status" => 403, "message" => "Key cannot be left empty!");
  }
