<?php
  error_reporting(E_ALL);
  ini_set('display_errors', 1);
  // start session
  session_start();

  // load config
  require_once('../config.inc.php');

  // Connect to magid
  require_once('../lib/php/easybitcoin/easybitcoin.php');
  $bitcoin = new Bitcoin($appConfig['Magid']['Username'],$appConfig['Magid']['Password'],$appConfig['Magid']['Host'],$appConfig['Magid']['Port']);

  // Load Class-FinlayDaG33k
  require_once('../lib/php/class-finlaydag33k/FinlayDaG33k.class.php');
  // connect to MySQL
  $conn = $FinlayDaG33k->Database->Connect($appConfig['MySQL']);

  // Connect to Redis
  require('../lib/php/predis/Autoloader.php');
  Predis\Autoloader::register();
  $redis = new Predis\Client([
    'scheme' => 'tcp',
    'host'   => '127.0.0.1',
    'port'   => 6379,
  ]);

  $doEncode = true;

  if(!$conn){
    $apiOutput = array("status" => 500, "message" => "Could not connect to MySQL Database");
  }else{
    switch($FinlayDaG33k->EzServer->getMethod()){
      case "POST":
        switch($_POST['action']){
          case "register":
            include('POST/register.php');
            break;
          case "login":
            include('POST/login.php');
            break;
          case "logout":
            include('POST/logout.php');
            break;
          case "importKey":
            include('POST/importKey.php');
            break;
          case "getTransactions":
            if($redis->exists("getTransactions " . $_SESSION['wallet-identifier'])){
              $apiOutput = $redis->get("getTransactions " . $_SESSION['wallet-identifier']);
              $doEncode = false;
            }else{
              include('POST/getTransactions.php');
              if($apiOutput['status'] == 200){
                $redis->set("getTransactions " . $_SESSION['wallet-identifier'],json_encode($apiOutput));
                $redis->expire("getTransactions " . $_SESSION['wallet-identifier'], 60);
              }
            }
            break;
          case "getWalletAddress":
            if($redis->exists("getWalletAddress " . $_SESSION['wallet-identifier'])){
              $apiOutput = $redis->get("getWalletAddress "  . $_SESSION['wallet-identifier']);
              $doEncode = false;
            }else{
              include('POST/getWalletAddress.php');
              if($apiOutput['status'] == 200){
                $redis->set("getWalletAddress " . $_SESSION['wallet-identifier'],json_encode($apiOutput));
                $redis->expire("getWalletAddress " . $_SESSION['wallet-identifier'], 60);
              }
            }
            break;
          case "getBalance":
            if($redis->exists("getBalance " . $_SESSION['wallet-identifier'])){
              $apiOutput = $redis->get("getBalance "  . $_SESSION['wallet-identifier']);
              $doEncode = false;
            }else{
              include('POST/getBalance.php');
              if($apiOutput['status'] == 200){
                $redis->set("getBalance " . $_SESSION['wallet-identifier'],json_encode($apiOutput));
                $redis->expire("getBalance " . $_SESSION['wallet-identifier'], 60);
              }
            }
            break;
          case "sendCoins":
            include('POST/sendCoins.php');
            break;
          case "changePassword":
            include('POST/changePassword.php');
            break;
          default:
            $apiOutput = array("status" => 501, "message" => "This request method is not available on this page.");
            break;
        }
        break;
      case "GET":
        switch($_GET['action']){
		  case "getBalance":
            if($redis->exists("getBalance")){
              $apiOutput = $redis->get("getBalance");
              $doEncode = false;
            }else{
              include('GET/getBalance.php');
              if($apiOutput['status'] == 200){
                $redis->set("getBalance",json_encode($apiOutput));
                $redis->expire("getBalance", 60);
              }
            }
            break;
          case "getConnections":
            if($redis->exists("getConnections")){
              $apiOutput = $redis->get("getConnections");
              $doEncode = false;
            }else{
              include('GET/getConnections.php');
              if($apiOutput['status'] == 200){
                $redis->set("getConnections",json_encode($apiOutput));
                $redis->expire("getConnections", 60);
              }
            }
            break;
          case "getMiningInfo":
            if($redis->exists("getMiningInfo")){
              $apiOutput = unserialize($redis->get("getMiningInfo"));
              $doEncode = false;
            }else{
              include('GET/getMiningInfo.php');
              if($apiOutput['status'] == 200){
                $redis->set("getMiningInfo",serialize(json_encode($apiOutput)));
                $redis->expire("getMiningInfo", 60);
              }
            }
            break;
          case "getWalletVersion":
            if($redis->exists("getWalletVersion")){
              $apiOutput = unserialize($redis->get("getWalletVersion"));
              $doEncode = false;
            }else{
              include('GET/getWalletVersion.php');
              if($apiOutput['status'] == 200){
                $redis->set("getWalletVersion",serialize(json_encode($apiOutput)));
                $redis->expire("getWalletVersion", 3600);
              }
            }
            break;
          case "getTicker":
            if($redis->exists("getTicker")){
              $apiOutput = unserialize($redis->get("getTicker"));
              $doEncode = false;
            }else{
              include('GET/getTicker.php');
              if($apiOutput['status'] == 200){
                $redis->set("getTicker",serialize(json_encode($apiOutput)));
                $redis->expire("getTicker", 60);
              }
            }
            break;
          case "searchBlock":
            if($redis->exists("searchBlock " . $_GET['blockID'])){
              $apiOutput = unserialize($redis->get("searchBlock " . $_GET['blockID']));
              $doEncode = false;
            }else{
              include('GET/searchBlock.php');
              if($apiOutput['status'] == 200){
                $redis->set("searchBlock " . $_GET['blockID'],serialize(json_encode($apiOutput)));
                $redis->expire("searchBlock " . $_GET['blockID'], 60);
              }
            }
            break;
          case "searchTx":
            if($redis->exists("searchTx " . $_GET['txID'])){
              $apiOutput = unserialize($redis->get("searchTx " . $_GET['txID']));
              $doEncode = false;
            }else{
              include('GET/searchTx.php');
              if($apiOutput['status'] == 200){
                $redis->set("searchTx " . $_GET['txID'],serialize(json_encode($apiOutput)));
                $redis->expire("searchTx " . $_GET['txID'], 60);
              }
            }
            break;
          case "validateAddress":
            if($redis->exists("validateAddress " . $_GET['address'])){
              $apiOutput = unserialize($redis->get("validateAddress " . $_GET['address']));
              $doEncode = false;
            }else{
              include('GET/validateAddress.php');
              if($apiOutput['status'] == 200){
                $redis->set("validateAddress " . $_GET['address'],serialize(json_encode($apiOutput)));
                $redis->expire("validateAddress " . $_GET['address'], 3600);
              }
            }
            break;
          case "getServerBalance":
            if($redis->exists("getServerBalance")){
              $apiOutput = $redis->get("getServerBalance");
              $doEncode = false;
            }else{
              include('GET/getServerBalance.php');
              if($apiOutput['status'] == 200){
                $redis->set("getServerBalance",json_encode($apiOutput));
                $redis->expire("getServerBalance", 60);
              }
            }
            break;
        }
        break;
      default:
        $apiOutput = array("status" => 501, "message" => "This request method is not available on this page.");
        break;
    }
  }


  // Enable CORS
  header("Access-Control-Allow-Origin: " . $FinlayDaG33k->EzServer->getRoot());
  // only do this if the data doesn't come from the Redis Database
  if($doEncode){
    echo json_encode($apiOutput);
  }else{
    echo $apiOutput;
  }
