<div class="row">
  <div class="col s12 m4 l4">
    <div class="card">
      <div class="card-content indigo white-text">
        <p class="card-title v-center">
          Block Height
        </p>
        <h4 class="card-number"><?= htmlentities($block['height']); ?></h4>
      </div>
    </div>
  </div>
  <div class="col s12 m4 l4">
    <div class="card">
      <div class="card-content indigo white-text">
        <p class="card-title v-center">
          Blockhash
        </p>
        <h4 class="card-number"><?= htmlentities(substr(ltrim($block['hash'],"0"),0,8)); ?></h4>
      </div>
    </div>
  </div>
  <div class="col s12 m4 l4">
    <div class="card">
      <div class="card-content indigo white-text">
        <p class="card-title v-center">
          Confirmations
        </p>
        <h4 class="card-number"><?= htmlentities($block['confirmations']); ?></h4>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col s12 m5 l5">
    <div class="card">
      <div class="card-content indigo white-text">
        <p class="card-title v-center">
          Summary
        </p>
        <table class="white-text responsive-table">
          <tbody>
            <tr>
              <td>Number Of Transactions</td>
              <td><?= count($block['tx']); ?></td>
            </tr>
            <tr>
              <td>Timestamp</td>
              <td><?= date("Y-m-d h:i:s",$block['time']); ?></td>
            </tr>
            <tr>
              <td>Difficulty</td>
              <td><?= htmlentities($block['difficulty']);?></td>
            </tr>
            <tr>
              <td>Size</td>
              <td>
                <?php
                  $sizeUnits = array("B","kB","mB");
                  echo htmlentities($FinlayDaG33k->Files->sizeToBinary($block['size'],$sizeUnits,2));
                ?>
              </td>
            </tr>
            <tr>
              <td>Bits</td>
              <td>
                <?= htmlentities(hexdec($block['bits'])); ?>
              </td>
            </tr>
            <tr>
              <td>Nonce</td>
              <td>
                <?= htmlentities($block['nonce']); ?>
              </td>
            </tr>
          </tbody>
        </table>
        <div class="clear"></div>
      </div>
    </div>
  </div>
  <div class="col s12 m7 l7">
    <div class="card">
      <div class="card-content indigo white-text">
        <p class="card-title v-center">
          Hashes
        </p>
        <div class="s12 m6 l6">
          <table class="white-text responsive-table">
            <tbody>
              <tr>
                <td>Hash</td>
                <td><a class="white-text" href="<?= htmlentities($FinlayDaG33k->EzServer->getHome());?>/search/<?= htmlentities($block['hash']); ?>"><?= htmlentities($block['hash']); ?></a></td>
              </tr>
              <tr>
                <td>Next Block(s)</td>
                <td>
                  <?php if(!empty($block['nextblockhash'])){ ?>
                    <a class="white-text" href="<?= htmlentities($FinlayDaG33k->EzServer->getHome());?>/search/<?= htmlentities($block['nextblockhash']); ?>"><?= htmlentities($block['nextblockhash']); ?></a>
                  <?php }else{ ?>
                    To be mined...
                  <?php } ?>
                </td>
              </tr>
              <tr>
                <td>Previous Block</td>
                <td><a class="white-text" href="<?= htmlentities($FinlayDaG33k->EzServer->getHome());?>/search/<?= htmlentities($block['previousblockhash']); ?>"><?= htmlentities($block['previousblockhash']); ?></a></td>
              </tr>
              <tr>
                <td>Merkle Root</td>
                <td><?= htmlentities($block['merkleroot']); ?></td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col s12 m12 l12">
    <div class="card">
      <div class="card-content indigo white-text">
        <p class="card-title v-center">
          Transactions
        </p>
        <div class="s12 m6 l6">
          <?php
            foreach($block['tx'] as $key => $tx){
              curl_setopt($ch, CURLOPT_URL, $FinlayDaG33k->EzServer->getHome() . "/ajax/index.php?action=searchTx&txID=" . urlencode($tx));
              $res = curl_exec($ch);
              $transaction = json_decode($res,1)['result'];
              ?>
                <div class="row">
                  <div class="col s12 m12 l13">
                    <hr>
                    <a class="white-text" href="<?= htmlentities($FinlayDaG33k->EzServer->getHome());?>/search/<?= htmlentities($tx); ?>"><?= htmlentities($tx); ?></a>
                    <hr>
                  </div>
                  <div class="col s6 m6 l6">
                    <?php
                      foreach($transaction['vin'] as $key2 => $vin){
                        curl_setopt($ch, CURLOPT_URL, $FinlayDaG33k->EzServer->getHome() . "/ajax/index.php?action=searchTx&txID=" . urlencode($vin['txid']));
                        $res = curl_exec($ch);
                        $inputTransaction = json_decode($res,1)['result'];
                        if(empty($inputTransaction)){
                          echo "Generation + Fees";
                        }else{
                          foreach($inputTransaction['vout'] as $txkey => $txin){
                            if($txin['n'] == $vin['vout']){
                              echo $txin['scriptPubKey']['addresses'][0] . " (Σ ".$txin['value'].")<br />";
                            }
                          }
                        }
                      }
                    ?>
                  </div>
                  <div class="col s6 m6 l6">
                    <?php
                      foreach($transaction['vout'] as $key => $vout){
                        echo $vout['scriptPubKey']['addresses'][0] . " (Σ ".$vout['value'].")<br />";
                      }
                    ?>
                  </div>
                </div>
              <?php
            }
          ?>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </div>
</div>
