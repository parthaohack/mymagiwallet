<div class="container">
  <div class="row">
    <div class="col l6 s12">
      <h5 class="white-text">Disclaimer</h5>
      <p class="grey-text text-lighten-4">This software is still in alpha-stages. proceed with caution!</p>
    </div>
    <div class="col l4 offset-l2 s12">
      <h5 class="white-text">Legal</h5>
      <ul>
        <li><a class="grey-text text-lighten-3" href="<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>/terms-of-service">Terms of Service</a></li>
      </ul>
    </div>
  </div>
</div>
<div class="footer-copyright">
  <div class="container">
  © 2018 Aroop "FinlayDaG33k" Roelofs | Support MyMagiWallet XMG: 9GZZM4NBQ6cFUeEdVF9ek81B6pxRveJy21 | issues? <a class="white-text" href="mailto:contact@finlaydag33k.nl">Contact me!</a>
  </div>
</div>
