<div id="wallet-receive" class="modal">
  <div class="modal-content">
    <h4>Receive Magicoin</h4>
    <p>
      <div class="row">
        <div class="row">
        <div class="col s12">
          <ul class="tabs">
            <li class="tab col s6">
              <a class="active indigo-text text-darken-4" href="#wallet-receive-text">Text</a>
            </li>
            <li class="tab col s6"><a class="active indigo-text text-darken-4" href="#wallet-receive-qrcode">QR Code</a></li>
            <div class="indicator indigo darken-4" style="z-index:1"></div>
          </ul>
        </div>
      </div>
        <div id="wallet-receive-text" class="col s12">
          <div class="row">
             <div class="input-field col s6 m6 l6">
                <input value="???" id="wallet-address" type="text" readonly="true">
                <label for="name">address</label>
             </div>
             <div class="input-field col s2 m2 l2">
                <button class="btn waves-effect waves-light clipboard-btn" data-clipboard-target="#wallet-address">Copy</button>
             </div>
          </div>
        </div>
        <div id="wallet-receive-qrcode" class="col s12">
          <div class="row">
            <center>
              <div id="qrcode"></div>
            </center>
          </div>
        </div>
      </div>
    </p>
    <script>
      UpdateUIItems.getWalletAddress = true;
      new Clipboard('.clipboard-btn');
      var qrcode = new QRCode("qrcode", {
        text: "<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>",
        width: 256,
        height: 256,
        colorDark : "#000000",
        colorLight : "#ffffff",
        correctLevel : QRCode.CorrectLevel.H
      });
    </script>
  </div>
</div>
