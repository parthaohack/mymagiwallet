<div class="row">
  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <h1 class="header center indigo-text">MyMagiWallet</h1>
      <div class="row center">
        <h5 class="header col s12 light">MagiCoin at your fingertips!</h5>
      </div>
      <div class="row center">
        <?php if(!empty($_SESSION['Authtoken'])){ ?>
          <a href="<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>/wallet" class="btn-large waves-effect waves-light indigo">Back to my wallet!</a>
        <?php }else{ ?>
          <a href="<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>/register" class="btn-large waves-effect waves-light indigo">Get Started</a>
        <?php } ?>

      </div>
    </div>
  </div>
</div>

<div class="row">
  <h2 class="header center indigo-text">Are you ready for...</h2>
  <div class="col s12 m4 l4">
    <div class="icon-block">
      <h2 class="center indigo-text"><i class="fa fa-refresh" aria-hidden="true"></i></h2>
      <h5 class="center">No more syncing!</h5>

      <p class="light">
        Tired of waiting for the blockchain to sync everytime you want to send some magi?<br />
        With MyMagiWallet, there is no need for you to sync the wallet anymore!<br />
        Just login and have access to all your MagiCoin in an instant!<br />
      </p>
    </div>
  </div>
  <div class="col s12 m4 l4">
    <div class="icon-block">
      <h2 class="center indigo-text"><i class="fa fa-wrench" aria-hidden="true"></i></h2>
      <h5 class="center">No more updating!</h5>

      <p class="light">
        MyMagiWallet will update it's wallet for you!<br />
        No more dealing with getting the latest and greatest MagiCoin wallet!<br />
        Stay up-to-date with the latest MagiCoin releases, without even having to think about it!
      </p>
    </div>
  </div>
  <div class="col s12 m4 l4">
    <div class="icon-block">
      <h2 class="center indigo-text"><i class="fa fa-cloud-download" aria-hidden="true"></i></h2>
      <h5 class="center">No huge blockchain!</h5>

      <p class="light">
        No longer you have to worry about a huge blockchain eating up your precious drive space!<br />
        MyMagiWallet does this for you, far away from you, you might even forget about the blockchain at all...
      </p>
    </div>
  </div>
</div>

<div class="row">
  <h2 class="header center indigo-text">Some juicy stats?</h2>
  <div class="col s12 m4 l4">
    <div class="icon-block">
      <h2 class="center indigo-text"><i class="fa fa-address-book" aria-hidden="true"></i></h2>
      <h5 class="center">Accounts</h5>
      <h5 class="center">???</h5>
    </div>
  </div>
  <div class="col s12 m4 l4">
    <div class="icon-block">
      <h2 class="center indigo-text"><i class="fa fa-paper-plane" aria-hidden="true"></i></h2>
      <h5 class="center">Transactions</h5>
      <h5 class="center">
		<?php
			
		?>
	  </h5>
    </div>
  </div>
  <div class="col s12 m4 l4">
    <div class="icon-block">
      <h2 class="center indigo-text">Σ</h2>
      <h5 class="center">Coins</h5>
      <h5 class="center">???</h5>
    </div>
  </div>
</div>
<div class="row center">
  <?php if(!empty($_SESSION['Authtoken'])){ ?>
    <a href="<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>/wallet" class="btn-large waves-effect waves-light indigo">Back to my wallet!</a>
  <?php }else{ ?>
    <a href="<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>/register" class="btn-large waves-effect waves-light indigo">Sign me up!</a>
  <?php } ?>
</div>
