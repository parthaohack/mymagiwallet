<?php
  if(!empty($_SESSION['Authtoken'])){
    echo "you are already logged in!<br />Please log-out before logging into another wallet!";
  }else{
    ?>
      <div class="row">
        <form id="loginForm" class="col s12 m12 l12">
          <div class="row">
            <div class="input-field col s12">
              <input id="email" type="text" class="validate" name="email">
              <label for="email">Email address</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input id="password" type="password" class="validate" name="password">
              <label for="password">Password</label>
              <small><a href="<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>/register">Don't have a wallet?</a></small>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <button type="submit" class="btn waves-effect waves-light"><i class="fa fa-paper-plane" aria-hidden="true"></i> Submit</button>
              <button type="reset" class="btn waves-effect waves-light"><i class="fa fa-times" aria-hidden="true"></i> reset</button>
            </div>
          </div>
        </form>
      </div>
      <script>
        $(document).ready(function() {
          $("#loginForm").submit(function(event) {
            event.preventDefault();
            $.ajax({
              url: apiURL,
              method: "POST",
              data: "action=login&" + $(this).serialize(),
              success: function (res) {
                console.log(res);
                data = JSON.parse(res);
                if(data.status == 200){
                  Materialize.toast('login complete!<br />Please hang on tight while we redirect you!', 4000);
                  window.location.href = "<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>/wallet";
                }else{
                  Materialize.toast(data.message, 4000);
                }
              }
            });
          });
        });
      </script>
    <?php
  }
