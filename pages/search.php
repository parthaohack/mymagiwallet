<?php
  // create curl resource
  $ch = curl_init();
  // set url
  curl_setopt($ch, CURLOPT_URL, $FinlayDaG33k->EzServer->getHome() . "/ajax/index.php?action=searchBlock&blockID=" . urlencode($_GET['q']));
  //return the transfer as a string
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  // $res contains the output string
  $res = curl_exec($ch);
  $block = json_decode($res,1);


  if($block['status'] == "200"){
    $block = $block['result'];
    include('components/displays/search_block.php');
  }else{
    curl_setopt($ch, CURLOPT_URL, $FinlayDaG33k->EzServer->getHome() . "/ajax/index.php?action=searchTx&txID=" . urlencode($_GET['q']));
    $res = curl_exec($ch);
    $tx = json_decode($res,1);
    if($tx['status'] == 200){
      $tx = $tx['result'];
      include('components/displays/search_tx.php');
    }else{
      ?>
        <div class="row">
          <div class="col s12">
            Could not find a block or Transaction with ID: <?= htmlentities($_GET['q']); ?><br />
            It might not exist, or not be picked up by our node yet...<br />
          </div>
        </div>
      <?php
    }
  }


  if(!$block && $tx){
    include('components/displays/search_tx.php');
  }

  // close curl resource to free up system resources
  curl_close($ch);
