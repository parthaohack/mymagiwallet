<?php if(!empty($_SESSION['wallet-identifier']) && !empty($_SESSION['Authtoken'])){ ?>
  <div class="row">
    <div class="col s12 m5 l5">
      <div class="card">
        <div class="card-content indigo white-text">
          <p class="card-title v-center">
            Σ Balance
          </p>
          <h4 class="card-number">Σ <span id="wallet-balance">0</span> <span id="wallet-balance-unconfirmed"></span></h4>
        </div>
        <div class="card-action indigo darken-1 white-text">
          <div class="left">
            <a class="waves-effect waves-light btn v-center modal-trigger" href="#wallet-send" onclick="$('#wallet-send').show();">
              <i class="material-icons">send</i> send
            </a>
          </div>
          <div class="right">
            <a class="waves-effect waves-light btn v-center modal-trigger" href="#wallet-receive" onclick="$('#wallet-receive').show();">
              <i class="material-icons">call_received</i> receive
            </a>
          </div>
          <div class="clear"></div>
        </div>
      </div>
    </div>
    <div class="col s12 m6 l3">
      <div class="card">
        <div class="card-content indigo white-text">
          <p class="card-title v-center">
            <i class="material-icons">attach_money</i> Ticker
          </p>
          <h4 class="card-number">
            US$ <span id="price-ticker">???</span>
          </h4>
        </div>
        <div class="card-action indigo darken-1">
          <center>
            <a href="https://coinmarketcap.com/currencies/magi/#markets" target="_blank" class="waves-effect waves-light btn">Coinmarketcap.com</a>
          </center>
        </div>
      </div>
    </div>
    <div class="col s12 m4 l4">
      <div class="card">
        <div class="card-content indigo white-text">
          <p class="card-title v-center">
            <i class="material-icons">attach_money</i> Wallet Value
          </p>
          <h4 class="card-number">
            US$ <span id="wallet-value">???</span>
          </h4>
        </div>
        <div class="card-action indigo darken-1 white-text">
          <center>
            Based on the price on the left
          </center>
        </div>
      </div>
    </div>
  </div>

  <div claas="row">
    <div class="col s12 m12 l12">
      <div class="card">
        <div class="card-content indigo white-text">
          <p class="card-title v-center">
            <i class="fa fa-money" aria-hidden="true"></i> Transactions
          </p>
          <table id="table-transactions" class="white-text responsive-table">
            <thead>
              <tr>
                <th>TXID</th>
                <th>Block</th>
                <th>Date/Time</th>
                <th>Address</th>
                <th>Amount</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <script>
  UpdateUIItems.getTransactions = true;
  UpdateUIItems.getTicker = true;
  UpdateUIItems.getBalance = true;
  </script>

  <script>
    $(function(){
      $('#wallet-receive').modal({
        ready: function(modal,trigger) { // Callback for Modal open. Modal and trigger parameters available.
          $.ajax({
            url: apiURL,
            method: "POST",
            data: "action=getWalletAddress",
            success: function (res) {
              data = JSON.parse(res);
              if(data.status == 200){
                $("#wallet-address").val(data.result);
                qrcode.makeCode(data.result);
              }else{
                $("#wallet-address").html("???");
              }
            }
          });
          UpdateUIItems.getWalletAddress = true;
        },
        complete: function() {
          UpdateUIItems.getWalletAddress = false;
        }
      });
      $('#wallet-send').modal();
    });
  </script>
<?php }else{
  ?>
    Something went wrong, you should be redirected soon!<br />
    Please click <a href="<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>/login">here</a> if this doesn't happen.
  <?php
  header("Location: " . $FinlayDaG33k->EzServer->getHome() . "/login");
}
